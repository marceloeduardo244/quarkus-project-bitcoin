// portal para criar projeto Quarkus
https://code.quarkus.io/

// instalar plugins quarkus
marcelo.oliveira@Z001340 MINGW64 /c/GitEstudo/microservicos/Quarkus/bitcoin
$ "C:\Program Files\apache-maven-3.9.1\bin\mvn.cmd" install quarkus:dev

// instalar graalVM
https://www.graalvm.org/downloads/

// instalar build tools para poder gerar a imagem
https://aka.ms/vs/15/release/vs_buildtools.exe

// instalando o native-image na graalVM

C:\Users\marcelo.oliveira\Downloads\instaladores\graalvm-ce-java11-windows-amd64-22.3.2\graalvm-ce-java11-22.3.2\bin>gu install native-image

// Criar a variavel de ambiente GRAALVM_HOME
Com a pasta home da graalvm na maquina

// rodar a imagem nativa (pelo powershell)
cmd /c 'call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvars64.bat" && mvn package -Pnative'