package br.com.alura.resource;

import java.util.List;
import java.util.Optional;

import br.com.alura.model.Ordem;
import br.com.alura.model.Usuario;
import br.com.alura.repository.UsuarioRepository;
import br.com.alura.service.OrdemService;
import br.com.alura.service.UsuarioService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.SecurityContext;

@Path("/ordens")
public class OrdensResource {
	
	@Inject
	OrdemService ordemService;
	
	@Inject
	UsuarioService usuarioService;
	
	@Inject
	UsuarioRepository usuarioRepository;
	
	@POST
	@Transactional 
	@RolesAllowed("user")
	@Consumes(MediaType.APPLICATION_JSON)
	public void inserir(@Context SecurityContext securityContext, Ordem ordem) {
		verificaAutenticidadeDaRequisicao(ordem, securityContext);
		
		ordemService.criarOrdem(ordem);
	}
	
	@GET
	@RolesAllowed("admin")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Ordem> listar() {
		return ordemService.getList();
	}
	
	public void verificaAutenticidadeDaRequisicao(Ordem ordem, SecurityContext securityContext) {
		Optional<Usuario> usuarioOptional = 
				usuarioRepository.findByIdOptional(ordem.getUserId());
		
		Usuario usuario = usuarioOptional.orElseThrow();
		if (!usuario.getUsername().equals(securityContext.getUserPrincipal().getName())) {
			throw new RuntimeException("O usuario logado é diferente do userId");
		}
	}
}
