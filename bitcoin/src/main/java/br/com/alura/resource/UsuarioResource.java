package br.com.alura.resource;

import java.util.List;

import br.com.alura.model.Usuario;
import br.com.alura.service.UsuarioService;
import jakarta.annotation.security.PermitAll;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/usuarios")
public class UsuarioResource {
	@Inject
	UsuarioService usuarioService;
	
	@POST
	@PermitAll
	@Transactional
	@Consumes(MediaType.APPLICATION_JSON)
	public void inserir(Usuario user) {
		usuarioService.criarUsuario(user);
	}
	
	@GET
	@RolesAllowed("admin")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Usuario> listar() {
		return usuarioService.getList();
	}
	
	@DELETE
	@RolesAllowed("admin")
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void listar(@PathParam("id") Long id) {
		usuarioService.apagarUsuarioPeloId(id);
	}
}
